var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifycss = require('gulp-minify-css');
var sass = require('gulp-sass');
var mainBowerFiles = require('gulp-main-bower-files');
var gulpFilter = require('gulp-filter');
var flatten = require('gulp-flatten');

var baseDir = 'web/assets/';


gulp.task('styles', function(){
    gulp.src([baseDir + 'src/styles/helis.scss'])
        .pipe(plumber({
            errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))
        .pipe(sass({
            includePaths: [
                './bower_components/bootstrap-sass/assets/stylesheets',
                './bower_components/sweetalert2/src'
            ]
        }))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(gulp.dest(baseDir + 'dist/styles/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest(baseDir + 'dist/styles/'));
});

gulp.task('scripts', function(){
    return gulp.src(baseDir + 'src/scripts/**/*.js')
        .pipe(plumber({
            errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))
        .pipe(concat('main.js'))
        .pipe(gulp.dest(baseDir + 'dist/scripts/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(baseDir + 'dist/scripts/'));
});

gulp.task('default', function(){
    gulp.watch(baseDir + "src/styles/**/*.scss", ['styles']);
    gulp.watch(baseDir + "src/scripts/**/*.js", ['scripts']);
});

gulp.task('vendor:scripts', function() {
    var filterJS = gulpFilter('**/*.js', { restore: true });

    return gulp.src('./bower.json')
        .pipe(mainBowerFiles())
        .pipe(filterJS)
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(baseDir + 'dist/scripts'));
});

gulp.task('vendor:styles', function() {
    var filterCSS = gulpFilter('**/*.css');

    return gulp.src('./bower.json')
        .pipe(mainBowerFiles())
        .pipe(filterCSS)
        .pipe(gulp.dest(baseDir + 'dist/styles/vendor'));
});

gulp.task('bootstrap:fonts', function () {
    return gulp.src(['./bower_components/bootstrap-sass/assets/fonts/bootstrap/*.{eot,svg,ttf,woff,woff2}'])
        .pipe(flatten())
        .pipe(gulp.dest(baseDir + 'dist/fonts/bootstrap'));
});