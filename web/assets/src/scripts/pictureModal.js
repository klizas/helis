(function ($) {
    'use strict';

    var $pictureModalTrigger = $('.trigger-picture-modal');
    var $body = $('body');

    $pictureModalTrigger.click(function (e) {
        e.preventDefault();

        var pictureId = $(this).data('picture-id');

        $.get(Routing.generate('picture_modal', {id: pictureId}), function (data) {
            var $retrievedHtml = $(data);

            $retrievedHtml.prependTo($body);

            var $pictureModal = $body.find('.picture-modal');
            var $removePictureTrigger = $pictureModal.find('.remove-picture');
            var $setAsAlbumCoverTrigger = $pictureModal.find('.set-cover');

            $removePictureTrigger.click(pictureRemoveHandler);
            $setAsAlbumCoverTrigger.click(makePictureAsCoverHandler);

            $pictureModal.modal({
                show: true
            });

            $pictureModal.on('hidden.bs.modal', function () {
                $pictureModal.remove();
            })
        });
    });

    function pictureRemoveHandler(e) {
        e.preventDefault();

        var $this = $(this);

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    var id = $this.closest('.modal').data('picture-id');

                    $.ajax({
                        url: Routing.generate('remove_picture', {id: id}),
                        type: 'DELETE',
                        success: function (data) {
                            resolve();
                        }
                    });
                })
            }
        }).then(function () {
            swal(
                'Deleted!',
                'Picture has been deleted.',
                'success'
            ).then(function () {
                window.location.reload();
            })
        });
    }

    function makePictureAsCoverHandler(e) {
        e.preventDefault();

        var id = $(this).closest('.modal').data('picture-id');

        $.get(Routing.generate('picture_set_cover', {id : id}), function () {
            swal(
                'Done!',
                'Album cover has been updated',
                'success'
            );
        })
    }
})(jQuery);