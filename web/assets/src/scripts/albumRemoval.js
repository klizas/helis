(function ($) {
    'use strict';

    var $removeAlbumTrigger = $('.remove-album');

    $removeAlbumTrigger.click(function (e) {
        e.preventDefault();

        var $this = $(this);

        swal({
            title: 'Are you sure?',
            text: "It will remove everything that is in album, from db, from file system.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    var slug = $this.data('slug');

                    $.ajax({
                        url: Routing.generate('remove_album', {slug: slug}),
                        type: 'DELETE',
                        success: function (data) {
                            resolve();
                        }
                    });
                })
            }
        }).then(function () {
            swal(
                'Deleted!',
                'Your album has been deleted.',
                'success'
            ).then(function () {
                window.location.href = Routing.generate('user_albums');
            })
        })
    })
})(jQuery);