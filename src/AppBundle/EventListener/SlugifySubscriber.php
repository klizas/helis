<?php

namespace AppBundle\EventListener;

use AppBundle\Util\Sluggable;
use Cocur\Slugify\Slugify;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class SlugifySubscriber implements EventSubscriber
{
    private $slugify;

    public function __construct(Slugify $slugify)
    {
        $this->slugify = $slugify;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
        );
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->slugify($args);
    }

    public function slugify(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if ($object instanceof Sluggable) {
            $entityManager = $args->getObjectManager();

            $title = $object->getTitle();
            $slug = $this->slugify->slugify($title) . '-' . substr(bin2hex(openssl_random_pseudo_bytes(32)), 0, 10);

            $object->setSlug($slug);
            $entityManager->flush();
        }
    }
}
