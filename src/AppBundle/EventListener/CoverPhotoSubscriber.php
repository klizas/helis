<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Picture;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class CoverPhotoSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
        );
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->setCoverPhoto($args);
    }

    public function setCoverPhoto(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if ($object instanceof Picture) {
            $album = $object->getAlbum();

            if ($album->getPictures()->count() == 1) {
                $entityManager = $args->getObjectManager();
                $album->setCoverPicture($object);
                $entityManager->flush();
            }
        }
    }
}
