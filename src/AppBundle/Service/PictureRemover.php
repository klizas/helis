<?php

namespace AppBundle\Service;

use AppBundle\Entity\Picture;
use Doctrine\ORM\EntityManager;

class PictureRemover
{
    private $picturesDir;
    private $em;

    public function __construct(EntityManager $entityManager, $picturesDir)
    {
        $this->em = $entityManager;
        $this->picturesDir = $picturesDir;
    }

    public function remove(Picture $picture)
    {
        $localFile = $this->picturesDir . '/' . $picture->getFile();

        if (file_exists($localFile)) {
            @unlink($localFile);

            $this->removeCoverFromAlbum($picture);

            $this->em->remove($picture);
            $this->em->flush();
        }
    }

    private function removeCoverFromAlbum(Picture $picture)
    {
        $album = $picture->getAlbum();

        if ($album->getCoverPicture() == $picture) {
            $album->setCoverPicture(null);
            $this->em->flush();
        }
    }
}
