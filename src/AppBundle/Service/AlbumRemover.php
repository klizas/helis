<?php

namespace AppBundle\Service;

use AppBundle\Entity\Album;
use Doctrine\ORM\EntityManager;

class AlbumRemover
{
    private $pictureRemover;
    private $em;

    public function __construct(PictureRemover $pictureRemover, EntityManager $entityManager)
    {
        $this->pictureRemover = $pictureRemover;
        $this->em = $entityManager;
    }

    public function remove(Album $album)
    {
        $this->recursiveRemovePictures($album);
        $this->em->remove($album);
        $this->em->flush();
    }

    private function recursiveRemovePictures(Album $album)
    {
        $childrenAlbums = $this->em->getRepository('AppBundle:Album')->getChildrenAlbums($album);
        $pictures = $album->getPictures();

        foreach ($pictures as $picture) {
            $this->pictureRemover->remove($picture);
        }

        foreach ($childrenAlbums as $cAlbum) {
            $this->recursiveRemovePictures($cAlbum);
        }

        $this->removeEmptySubAlbums($album);
    }

    private function removeEmptySubAlbums(Album $album)
    {
        $empty = true;

        $childrenAlbums = $this->em->getRepository('AppBundle:Album')->getChildrenAlbums($album);

        foreach ($childrenAlbums as $cAlbum) {
            $empty &= $this->removeEmptySubAlbums($cAlbum);
        }

        return $empty && $this->removeAlbum($album);
    }

    private function removeAlbum(Album $album)
    {
        $this->em->remove($album);
        $this->em->flush();

        return true;
    }
}
