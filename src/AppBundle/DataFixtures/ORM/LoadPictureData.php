<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Album;
use AppBundle\Entity\Picture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Faker;


class LoadPictureData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    const IMAGE_API = 'https://unsplash.it/1000/1000/?random';
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user = $this->getReference('testUser');
        $faker = Faker\Factory::create();

        $i = 1;

        while ($i <= 15) {
            $picture = new Picture();
            $album = $this->getReference('album-' . ceil($i / 5));

            $picture->setTitle($faker->word);
            $picture->setUser($user);
            $picture->setAlbum($album);
            $picture->setDescription($faker->sentence());
            $picture->setIsPrivate($faker->boolean(20));

            $image = $this->downloadRandomImage();

            $picture->setFile($image);

            $manager->persist($picture);
            $manager->flush();

            $i++;
        }
    }

    public function downloadRandomImage()
    {
        $dir = $this->container->getParameter('pictures_dir');

        $filename = md5(uniqid()) . '.jpg';

        if ($imgContent = file_get_contents(self::IMAGE_API)) {
            file_put_contents($dir . '/' . $filename, $imgContent);
        } else {
            return $this->downloadRandomImage();
        }

        return $filename;
    }

    public function getOrder()
    {
        return 3;
    }
}
