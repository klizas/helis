<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Album;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Faker;


class LoadAlbumData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user = $this->getReference('testUser');
        $faker = Faker\Factory::create();

        $i = 1;

        while ($i <= 3) {
            $album = new Album();
            $album->setTitle($faker->word);
            $album->setUser($user);

            $manager->persist($album);
            $manager->flush();

            $this->addReference('album-' . $i, $album);
            $i++;
        }
    }

    public function getOrder()
    {
        return 2;
    }
}
