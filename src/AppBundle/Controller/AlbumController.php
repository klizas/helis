<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use AppBundle\Form\AlbumType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AlbumController extends Controller
{
    /**
     * @Route("/library", name="user_albums", options={"expose"=true})
     * @Security("has_role('ROLE_USER')")
     */
    public function userAlbumsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $albums = $em->getRepository('AppBundle:Album')->getTopLevelAlbumsByUser($user);

        return $this->render('AppBundle:Album:user_albums.html.twig', [
            'albums' => $albums
        ]);
    }

    /**
     * @param $request Request
     * @Route("/library/new-album", name="new_album")
     * @Security("has_role('ROLE_USER')")
     * @return Response
     */
    public function newAlbumAction(Request $request)
    {
        $album = new Album();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $album->setUser($user);

        $form = $this->createForm(AlbumType::class, $album);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($album);
            $em->flush();

            return $this->redirect($this->generateUrl('single_album', ['slug' => $album->getSlug()]));
        }

        return $this->render('@App/Album/new_album.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param $slug string
     *
     * @Method("GET")
     * @Route("/albums/{slug}", name="single_album")
     *
     * @return Response
     */
    public function singleAlbumAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $album = $em->getRepository('AppBundle:Album')->findOneBy(['slug' => $slug]);

        if (!$album) {
            throw $this->createNotFoundException("Album not found");
        }

        $onlyPublic = ($album->getUser() != $user);

        $albums = $em->getRepository('AppBundle:Album')->getChildrenAlbums($album);
        $pictures = $em->getRepository('AppBundle:Picture')->getAlbumPictures($album, $onlyPublic);

        return $this->render('AppBundle:Album:album.html.twig', [
            'album' => $album,
            'albums' => $albums,
            'pictures' => $pictures,
            'viewingAsOwner' => (!$onlyPublic)
        ]);
    }

    /**
     * @param $slug string
     *
     * @Method("DELETE")
     * @Security("has_role('ROLE_USER')")
     * @Route("/albums/{slug}", name="remove_album", options={"expose"=true})
     *
     * @return JsonResponse
     */
    public function removeAlbumAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $album = $em->getRepository('AppBundle:Album')->findOneBy(['slug' => $slug]);

        if (!$album) {
            throw $this->createNotFoundException();
        }

        if ($user != $album->getUser()) {
            throw $this->createAccessDeniedException();
        }

        $albumRemoval = $this->get('app.album_remover');
        $albumRemoval->remove($album);

        return new JsonResponse([], 200);
    }
}
