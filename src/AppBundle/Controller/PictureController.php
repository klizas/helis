<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Picture;
use AppBundle\Form\PictureType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PictureController extends Controller
{
    /**
     * @param int $id
     *
     * @Route("/pictures/{id}/modal", name="picture_modal", options={"expose"=true}, requirements={
     *     "id" : "\d+"
     * })
     * @return Response|NotFoundHttpException
     */
    public function singlePictureModalAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $picture = $em->getRepository('AppBundle:Picture')->find($id);

        if (!$picture) {
            throw $this->createNotFoundException("Picture Not Found");
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $isViewedByOwner = ($picture->getUser() == $user);

        if ($picture->getIsPrivate() && (!$isViewedByOwner)) {
            throw $this->createAccessDeniedException("This picture is private");
        }

        return $this->render('@App/Picture/picture_modal.html.twig', [
            'picture' => $picture,
            'viewedByOwner' => $isViewedByOwner
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/library/new-picture", name="new_picture")
     * @return Response
     */
    public function newPictureAction(Request $request)
    {
        $picture = new Picture();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

        $userHasAlbums = $em->getRepository('AppBundle:Album')->findBy(['user' => $user]);

        if (!$userHasAlbums) {
            return $this->render('@App/Picture/new_picture_no_albums.html.twig');
        }

        $form = $this->createForm(PictureType::class, $picture);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $picture->setUser($user);

            $file = $picture->getFile();

            $fileName = $this->get('app.picture_uploader')->upload($file);

            $picture->setFile($fileName);

            $em->persist($picture);
            $em->flush();

            return $this->redirect($this->generateUrl('single_album', ['slug' => $picture->getAlbum()->getSlug()]));
        }

        return $this->render('@App/Picture/new_picture.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param $id int
     *
     * @Method("DELETE")
     * @Security("has_role('ROLE_USER')")
     * @Route("/picture/{id}", name="remove_picture", options={"expose"=true}, requirements={
     *     "id" : "\d+"
     * })
     *
     * @return JsonResponse
     */
    public function removePictureAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $picture = $em->getRepository('AppBundle:Picture')->find($id);

        if (!$picture) {
            throw $this->createNotFoundException();
        }

        if ($user != $picture->getUser()) {
            throw $this->createAccessDeniedException();
        }

        $albumRemoval = $this->get('app.picture_remover');
        $albumRemoval->remove($picture);

        return new JsonResponse([], 200);
    }

    /**
     * @param $id int
     *
     * @Security("has_role('ROLE_USER')")
     * @Route("/picture/{id}/cover", name="picture_set_cover", options={"expose"=true}, requirements={
     *     "id" : "\d+"
     * })
     *
     * @return JsonResponse
     */
    public function setPictureAsCoverAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $picture = $em->getRepository('AppBundle:Picture')->find($id);

        if (!$picture) {
            throw $this->createNotFoundException();
        }

        if ($user != $picture->getUser()) {
            throw $this->createAccessDeniedException();
        }

        $picture->getAlbum()->setCoverPicture($picture);

        $em->flush();

        return new JsonResponse([], 200);
    }
}
