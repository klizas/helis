<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $pictures = $em->getRepository('AppBundle:Picture')->getAllPublicPictures();

        return $this->render('@App/Home/index.html.twig', [
            'pictures' => $pictures
        ]);
    }
}
