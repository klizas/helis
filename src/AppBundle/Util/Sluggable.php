<?php

namespace AppBundle\Util;

interface Sluggable
{
    public function getTitle();

    public function getSlug();

    public function setSlug($slug);
}