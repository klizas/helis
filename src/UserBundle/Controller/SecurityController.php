<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Form\ChangePasswordType;
use UserBundle\Form\Model\ChangePassword;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="user_login")
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('@User/Login/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/change-password", name="change_password")
     */
    public function changePasswordAction(Request $request)
    {
        $changePasswordModel = new ChangePassword();
        $form = $this->createForm(ChangePasswordType::class, $changePasswordModel);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->get('user.manager');
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $manager->changePassword($user, $form->get('newPassword')->getData());

            $this->addFlash(
                'success',
                'Your password has been changed.'
            );
        }

        return $this->render('@User/ChangePassword/change_password.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @Route("/logout", name="user_logout")
     */
    public function logoutAction(Request $request)
    {
    }
}
