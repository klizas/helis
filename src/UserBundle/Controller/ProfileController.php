<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends Controller
{
    /**
     * @param $request Request
     * @return Response
     * @Route("/user/{username}", name="user_profile")
     */
    public function userProfileAction($username)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('UserBundle:User')->findOneBy(['username' => $username]);

        if (!$user) {
            throw $this->createNotFoundException("User not found");
        }

        $albums = $em->getRepository('AppBundle:Album')->getTopLevelAlbumsByUser($user);

        return $this->render('@User/Profile/user_profile.html.twig', [
            'user' => $user,
            'albums' => $albums
        ]);
    }
}
