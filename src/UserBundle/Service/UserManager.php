<?php

namespace UserBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use UserBundle\Entity\User;

class UserManager
{
    private $encoder;
    private $em;

    public function __construct(UserPasswordEncoder $encoder, EntityManager $entityManager)
    {
        $this->encoder = $encoder;
        $this->em = $entityManager;
    }

    public function changePassword(User $user, $plainPassword)
    {
        $encodedPassword = $this->encoder->encodePassword($user, $plainPassword);
        $user->setPassword($encodedPassword);

        $this->em->flush();
    }
}
