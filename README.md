SeoHelis User Pictures task
=====

## Paleidimas
Tam kad užsikurti projektą, reikia atlikti tokius veiksmus:

    git clone git@bitbucket.org:klizas/helis.git
    cd helis/
    composer install
    php bin/console do:da:cr
    php bin/console do:sc:cr
    php bin/console do:fi:lo

Paskutinės komandos vykdymas gali užtrukti, nes yra parsiunčiami paveikslėliai iš nutolusio serviso.
Šios komandos metu sukuriami fiktyvūs albumai, paveikslėliai ir vartotojas **testUser** su slaptažodžiu **password**.

Paleidžiame integruotą serverį:

    php bin/console ser:start

## Testavimas
``phpunit``