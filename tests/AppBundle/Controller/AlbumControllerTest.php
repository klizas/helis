<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AlbumControllerTest extends WebTestCase
{
    public function testUserAlbumsNotAuthenticated()
    {
        $client = static::createClient();

        $client->request('GET', '/library');

        // If user is not authenticated, its redirected to /login
        $this->assertEquals(
            302,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testUserAlbumsAuthenticated()
    {
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'testUser',
            'PHP_AUTH_PW'   => 'password',
        ));

        $crawler = $client->request('GET', '/library');

        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );

        $this->assertGreaterThan(
            0,
            $crawler->filter('h1:contains("Your Library")')->count()
        );
    }

    public function testCreateNewAlbum()
    {
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'testUser',
            'PHP_AUTH_PW'   => 'password',
        ));

        $crawler = $client->request('GET', '/library/new-album');

        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode(),
            'Unexpected HTTP status code for GET /library/new-album'
        );

        $form = $crawler->selectButton('Create')->form(array(
            'appbundle_album[title]' => 'Test',
        ));

        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect());
        $crawler = $client->followRedirect();

        $this->assertGreaterThan(
            0,
            $crawler->filter('h1:contains("Test")')->count()
        );
    }

    public function testRemoveAlbum()
    {
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'testUser',
            'PHP_AUTH_PW'   => 'password',
        ));

        $crawler = $client->request('GET', '/library');

        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode(),
            'Unexpected HTTP status code for GET /library'
        );

        $this->assertGreaterThan(
            0,
            $crawler->filter('a:contains("Test")')->count()
        );

        $testAlbumLink = $crawler
            ->filter('a:contains("Test")')
            ->first()
            ->link();

        $crawler = $client->click($testAlbumLink);

        $this->assertGreaterThan(
            0,
            $crawler->filter('h1:contains("Test")')->count()
        );

        $albumUrl = $client->getHistory()->current()->getUri();

        $client->request('DELETE', $albumUrl);

        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode(),
            'Unexpected HTTP status code for DELETE ' . $albumUrl
        );


        $client->request('GET', $albumUrl);

        $this->assertEquals(
            404,
            $client->getResponse()->getStatusCode(),
            'Unexpected HTTP status code for GET ' . $albumUrl
        );
    }
}
